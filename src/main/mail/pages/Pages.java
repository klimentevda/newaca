package mail.pages;

import mail.pages.mail.LoginPage;
import mail.pages.mail.MailPage;



import static mail.selenium.SeleniumFactory.init;


public class Pages {

    private static ThreadLocal<Pages> instance = new ThreadLocal<>();

    public static Pages get() {
        if (instance.get() == null) {
            instance.set(new Pages());
        }
        return instance.get();
    }

    public final LoginPage loginPage;
    public final MailPage mailPage;

    private Pages() {
        mailPage = init(MailPage.class);
        loginPage = init(LoginPage.class);
    }

}
