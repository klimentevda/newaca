package mail.pages.mail;


import mail.pages.Pages;
import mail.pages.mail.tabs.IncomingPage;
import mail.ui.initdriver.Element;
import org.openqa.selenium.support.PageFactory;


public class MailPage extends Element {

    //---tabs----
    public final Tabs tabs = new Tabs();

    public final class Tabs{
        public final IncomingPage incomingPage = PageFactory.initElements(driver, IncomingPage.class);
    }

}
