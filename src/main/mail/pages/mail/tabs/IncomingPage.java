package mail.pages.mail.tabs;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class IncomingPage {

    @FindBy(id = "PH_user-email")
    private WebElement emailUserLink;

    @FindBy(id = "PH_logoutLink")
    private WebElement exitButton;


    public WebElement getEmailUserLink() {
        return emailUserLink;
    }

    public WebElement getExitButton() {
        return exitButton;
    }
}
