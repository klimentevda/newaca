package mail.reporter.extentreport;

import com.relevantcodes.extentreports.LogStatus;

import mail.ui.initdriver.InitialDriver;

public class ExtentScreen {

    public void getScreen(String screen) {
        if (InitialDriver.getInstance().driver == null) return;
        //Get driver from BaseTest and assign to local webdriver variable.
        if (ExtentTestManager.getTest() != null) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "Test Failed",
                    ExtentTestManager.getTest().addBase64ScreenShot(screen));
        }


    }
}
