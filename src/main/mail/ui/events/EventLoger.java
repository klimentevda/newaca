package mail.ui.events;

import com.relevantcodes.extentreports.LogStatus;
import mail.ui.initdriver.Element;
import mail.ui.initdriver.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

import mail.reporter.extentreport.ExtentTestManager;

public  class EventLoger implements WebDriverEventListener {

	private Element element = new Element();

	@Override
	public void beforeAlertAccept(WebDriver webDriver) {

	}

	@Override
	public void afterAlertAccept(WebDriver webDriver) {

	}

	@Override
	public void afterAlertDismiss(WebDriver webDriver) {

	}

	@Override
	public void beforeAlertDismiss(WebDriver webDriver) {

	}

	@Override
	public void beforeNavigateTo(String s, WebDriver webDriver) {
		if (!Logger.getInstance().loggerNavigateTxt.contains("Go to URL:" + s)) {
			if (ExtentTestManager.getTest() != null) {
				ExtentTestManager.getTest().log(LogStatus.INFO, "Go to URL:" + s);
			}
		} else {
			Logger.getInstance().loggerNavigateTxt = "Go to URL:" + s;
		}
	}

	@Override
	public void afterNavigateTo(String s, WebDriver webDriver) {

	}

	@Override
	public void beforeNavigateBack(WebDriver webDriver) {

	}

	@Override
	public void afterNavigateBack(WebDriver webDriver) {

	}

	@Override
	public void beforeNavigateForward(WebDriver webDriver) {

	}

	@Override
	public void afterNavigateForward(WebDriver webDriver) {

	}

	@Override
	public void beforeNavigateRefresh(WebDriver webDriver) {

	}

	@Override
	public void afterNavigateRefresh(WebDriver webDriver) {

	}

	@Override
	public void beforeFindBy(By by, WebElement webElement, WebDriver webDriver) {

	}

	@Override
	public void afterFindBy(By by, WebElement webElement, WebDriver webDriver) {

	}

	@Override
	public void beforeClickOn(WebElement webElement, WebDriver webDriver) {
		element.waitUntilClickable(webElement);
	}



	@Override
	public void afterClickOn(WebElement webElement, WebDriver webDriver) {
		if (!Logger.getInstance().loggerClickTxt.contains("Click on element: " + webElement)) {
			if(ExtentTestManager.getTest() != null){
				ExtentTestManager.getTest().log(LogStatus.INFO, "Click on element: " + webElement);
			}
		} else {
			Logger.getInstance().loggerClickTxt = "Click on element: " + webElement;
		}

	}

	@Override
	public void beforeChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {
		if (!Logger.getInstance().loggerValueTxt.contains("Change value on : " + charSequences + " in element" + webElement)) {
			if (ExtentTestManager.getTest() != null){
				ExtentTestManager.getTest().log(LogStatus.INFO, "Change value on : " + charSequences + " in element" + webElement);
			}
		} else {
			Logger.getInstance().loggerValueTxt = "Change value on : " + charSequences + " in element" + webElement;
		}
	}

	@Override
	public void afterChangeValueOf(WebElement webElement, WebDriver webDriver, CharSequence[] charSequences) {

	}

	@Override
	public void beforeScript(String s, WebDriver webDriver) {

	}

	@Override
	public void afterScript(String s, WebDriver webDriver) {

	}

	@Override
	public void beforeSwitchToWindow(String s, WebDriver webDriver) {

	}

	@Override
	public void afterSwitchToWindow(String s, WebDriver webDriver) {

	}

	@Override
	public void onException(Throwable throwable, WebDriver webDriver) {

	}

	@Override
	public <X> void beforeGetScreenshotAs(OutputType<X> outputType) {

	}

	@Override
	public <X> void afterGetScreenshotAs(OutputType<X> outputType, X x) {

	}

	@Override
	public void beforeGetText(WebElement webElement, WebDriver webDriver) {

	}

	@Override
	public void afterGetText(WebElement webElement, WebDriver webDriver, String s) {

	}


}
