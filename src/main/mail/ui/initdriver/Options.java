package mail.ui.initdriver;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerOptions;

abstract class Options {

	ChromeOptions chromeOptions() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--window-size=1920,1080");
		options.addArguments("--disable-infobars");
		options.addArguments("--no-sandbox");
		return options;
	}

	FirefoxOptions firefoxOptions() {
		FirefoxOptions options = new FirefoxOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--window-size=1920,1080");
		options.addArguments("--disable-infobars");
		options.addArguments("--no-sandbox");
		return options;
	}

	InternetExplorerOptions internetExplorerOptions() {
		InternetExplorerOptions options = new InternetExplorerOptions();
		return options;
	}
}
