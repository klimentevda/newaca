package mail.ui.initdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;

public class Element {

    protected final WebDriver driver = InitialDriver.getInstance().getDriver();

    /**
     * Status of element
     * Get status of element
     *
     * @param webElementExpectedCondition WebElement element,By locator
     * @return getWebElement(ExpectedConditions.elementToBeClickable ( element));
     */
    public WebElement getWebElement(ExpectedCondition<WebElement> webElementExpectedCondition) {
        return waitElement().until(webElementExpectedCondition);
    }

    /**
     * It webDriver wait for all element
     *
     * @return
     */
    private WebDriverWait waitElement() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.pollingEvery(Duration.ofMillis(10));
        wait.ignoring(NoSuchElementException.class);
        wait.ignoring(StaleElementReferenceException.class);
        wait.ignoring(InterruptedException.class);
        return wait;
    }

    /**
     * @return WebDriverWait with element wait
     */
    public static WebDriverWait getElementWait() {
        WebDriverWait wait = new WebDriverWait(InitialDriver.getInstance().getDriver(),
                20);
        return wait;
    }


    public WebElement waitUntilClickable(WebElement element) {
        return getWebElement(ExpectedConditions.elementToBeClickable(element));
    }

    public void notSuchProtectedClick(WebElement element) {
        try {
            element.click();
        } catch (NoSuchElementException e) {
            waitUntilClickable(element);
            element.click();
        }

    }

    public String notSuchProtectedGetText(WebElement element) {
        try {
            return element.getText();
        }catch (NoSuchElementException e){
            waitUntilClickable(element);
            return element.getText();
        }
    }

    public void notSuchProtectedSedKeys(WebElement element , String text) {
        try {
            element.sendKeys(text);
        }catch (NoSuchElementException e){
            waitUntilClickable(element);
            element.sendKeys(text);
        }
    }
}
