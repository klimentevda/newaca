package mail.ui.initdriver;

import org.openqa.selenium.support.ui.WebDriverWait;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import mail.ui.events.EventHandler;
import mail.ui.events.EventLoger;

import java.nio.file.Path;
import java.nio.file.Paths;


import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;
import static mail.deserializers.Base.DRIVER_NAME;
import static mail.deserializers.Base.DRIVER_PATH;
import static mail.deserializers.Base.DRIVER_VERSION;


public final class InitialDriver extends Options {

	public static InitialDriver getInstance() {
		if (driverThread.get() == null) {
			synchronized (InitialDriver.class) {
				driverThread.set(new InitialDriver());
			}
		}
		return driverThread.get();
	}

	/*
	 * There is pre-initialization of the driver and his way that is it prior to calling object
	 */
	private static ThreadLocal<InitialDriver> driverThread = new ThreadLocal<>();

	public WebDriver driver;

	public WebDriver getDriver() {
		if (driver == null) {
			driver = initialDriver();
			return driver;
		} else {
			return driver;
		}
	}

	/**
	 * Return web driver existing for specific thread
	 **/
	public static WebDriver currentDriver() {
		return getInstance().getDriver();
	}



	/*
	 * There is setting driver by name
	 */
	private synchronized WebDriver initialDriver() {
		switch (DRIVER_NAME) {
			case "CHROME": {

				if (!DRIVER_VERSION.equals("0"))
				{
					WebDriverManager.chromedriver().version(DRIVER_VERSION).setup();
					ChromeDriverManager.getInstance(CHROME).setup();
				} else {
					System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
				}
				driver = new ChromeDriver(chromeOptions());
				break;
			}
			case "FIREFOX": {
				WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver(firefoxOptions());
				break;
			}
			case "IE": {
				driver = new InternetExplorerDriver(internetExplorerOptions());
				break;
			}

			default:
			{
				System.setProperty("webdriver.chrome.driver", DRIVER_PATH);
				driver = new ChromeDriver(chromeOptions());
			}
		}

		EventFiringWebDriver eventDriver = new EventFiringWebDriver(driver);
		EventHandler handler = new EventHandler() {
		};
		EventLoger log = new EventLoger() {
		};
		driver = eventDriver.register(log);
		driver = eventDriver.register(handler);
		return driver;
	}


	public void destroy() {
		if (driver != null) {
			getDriver().quit();
			driver = null;
		}
	}


}
