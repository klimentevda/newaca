package mail.listner;

import com.relevantcodes.extentreports.LogStatus;
import mail.reporter.Log;
import mail.reporter.extentreport.ExtentManager;
import mail.reporter.extentreport.ExtentScreen;
import mail.reporter.extentreport.ExtentTestManager;
import mail.ui.initdriver.InitialDriver;
import mail.utils.UtilsMethods;
import org.testng.*;


public class Listener implements ITestListener, ISuiteListener{

    @Override
    public void onTestStart(ITestResult arg0) {
        Log.info("I am in onTestStart method " + getTestMethodName(arg0) + " start");
            ExtentTestManager.startTest(arg0.getName(), "");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        Log.info("I am in onTestSuccess method " + getTestMethodName(iTestResult) + " succeed");
    }

    @Override
    public void onTestFailure(ITestResult arg0) {
        Log.info("I am in onTestFailure method " + getTestMethodName(arg0) + " failed");
        ExtentTestManager.getTest().log(LogStatus.FAIL, "Test failed");
        UtilsMethods.getInstance().screenFullPage(arg0);
        String screen= UtilsMethods.getInstance().createScreen();
        new ExtentScreen().getScreen(screen);
        InitialDriver.getInstance().destroy();
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        Log.info("I am in onTestSkipped method " + getTestMethodName(iTestResult) + " skipped");
        ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped");
        InitialDriver.getInstance().destroy();
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        Log.info("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
    }

    @Override
    public void onStart(ISuite arg0) {
        Log.info("I am in onStart method " + arg0.getName());
        if (!arg0.getName().contains("Default Suite")) {
            ExtentManager.getReporter(arg0.getName()).startTest(arg0.getName());
        }

    }



    @Override
    public void onFinish(ISuite arg0) {
        Log.info("I am in onFinish method " + arg0.getName());
        ExtentTestManager.endTest(arg0.getName());
        ExtentManager.getReporter(arg0.getName()).flush();
        InitialDriver.getInstance().destroy();
    }


    @Override
    public void onStart(ITestContext arg0) {

    }

    @Override
    public void onFinish(ITestContext arg0) {
        InitialDriver.getInstance().destroy();

    }

    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

}