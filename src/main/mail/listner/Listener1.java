package mail.listner;

import com.relevantcodes.extentreports.LogStatus;
import io.qameta.allure.AllureUtils;
import mail.reporter.extentreport.ExtentManager;
import mail.reporter.extentreport.ExtentScreen;
import mail.reporter.extentreport.ExtentTestManager;
import mail.ui.initdriver.InitialDriver;
import mail.utils.UtilsMethods;
import org.testng.*;
import org.testng.annotations.Listeners;


@Listeners({Listener.class})
public class Listener1 implements ITestListener, ISuiteListener, IInvokedMethodListener {

    @Override
    public void onStart(ISuite arg0) {
        if (!arg0.getName().contains("Default Suite")) {
            ExtentManager.getReporter(arg0.getName()).startTest(arg0.getName());
        }

    }

    @Override
    public void onFinish(ISuite arg0) {
        ExtentTestManager.endTest(arg0.getName());
        ExtentManager.getReporter(arg0.getName()).flush();
        InitialDriver.getInstance().destroy();
    }

    @Override
    public void onStart(ITestContext arg0) {

    }

    @Override
    public void onFinish(ITestContext arg0) {

    }

    @Override
    public void onTestSuccess(ITestResult arg0) {
        ExtentTestManager.getTest().log(LogStatus.PASS, "Test passed");
        InitialDriver.getInstance().destroy();

    }

    @Override
    public void onTestFailure(ITestResult arg0) {
        ExtentTestManager.getTest().log(LogStatus.FAIL, "Test failed");
        UtilsMethods.getInstance().screenFullPage(arg0);
        String screen = UtilsMethods.getInstance().createScreen();
        new ExtentScreen().getScreen(screen);
        InitialDriver.getInstance().destroy();


    }

    @Override
    public void onTestStart(ITestResult arg0) {
        ExtentTestManager.startTest(arg0.getName(), "");
    }

    @Override
    public void onTestSkipped(ITestResult arg0) {
        ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Skipped");
        InitialDriver.getInstance().destroy();

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {

    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {

    }
}