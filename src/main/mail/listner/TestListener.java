package mail.listner;

import mail.reporter.Log;
import org.testng.IInvokedMethod;
import org.testng.ITestNGListener;
import org.testng.ITestResult;

public class TestListener implements ITestNGListener {

    private static final int SUCCESS_STATUS = 1;
    private static final int FAILED_STATUS = 2;
    private static final int SKIPPED_STATUS = 3;

    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        switch (iTestResult.getStatus()) {
            case SUCCESS_STATUS:
                Log.info("[TEST SUCCESS]" + iInvokedMethod.getTestMethod().getMethodName());

            case FAILED_STATUS:
                Log.error("[TEST FAILED]" + iInvokedMethod.getTestMethod().getMethodName());

            case SKIPPED_STATUS:
               Log.debug("[TEST STATUS]" + iInvokedMethod.getTestMethod().getMethodName());
        }
    }


}
