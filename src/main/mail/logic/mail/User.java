package mail.logic.mail;


import mail.logic.BaseTest;
import mail.pages.Pages;
import mail.ui.initdriver.Element;
import mail.ui.seleniumelements.Windows;

public class User extends BaseTest {

    private Windows Chrome = new Windows();
    private Element element = new Element();

    public void OpenMail() {
        Chrome.navigate(URL);
    }

    public void LeaveMail() {
        element.notSuchProtectedClick(Pages.get().mailPage.tabs.incomingPage.getExitButton());
    }

    public void loginIn() {

        element.notSuchProtectedSedKeys(Pages.get().loginPage.getLoginInput(), USER_NAME);
        element.notSuchProtectedSedKeys(Pages.get().loginPage.getPasswordInput(),USER_PASSWORD);
        element.notSuchProtectedClick(Pages.get().loginPage.getComInButton());

    }
}
