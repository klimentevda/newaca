package mail.utils;


import mail.ui.initdriver.InitialDriver;
import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.testng.ITestResult;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilsMethods {

    private WebDriver driverLocal= InitialDriver.getInstance().driver;
    private static UtilsMethods Instance;

    public static UtilsMethods getInstance()
    {
        synchronized (UtilsMethods.class)
        {
            if(Instance==null)
            {
                Instance= new UtilsMethods();
            }
        }
        return Instance;
    }

    public String screenFullPage(ITestResult arg)
    {
        try {
            final Screenshot screenshot = new AShot()
                    .shootingStrategy(ShootingStrategies.viewportPasting(100))
                    .takeScreenshot(driverLocal);
            final BufferedImage image = screenshot.getImage();
            String path="src\\resources\\ScreenShot";
            File file = new File(path);
            if(!file.exists())
                {
                  file.mkdir();
                }
            SimpleDateFormat format = new SimpleDateFormat("HH_mm_dd-MM-yyyy");
            String dateString = format.format( new Date()   );
            ImageIO.write(image, "PNG", new File(path+"\\"
                    + arg.getName()+"_"+dateString+".png"));
            return new String(Base64.encodeBase64(screenshot.toString().getBytes()), "UTF-8");
        }
        catch (IOException ex)
        {
            return  ex.getMessage();
        }
    }
    /**
     *Create Screen
     */
    public String createScreen()
    {
        try {
            WebDriver augmentedDriver = new Augmenter().augment(driverLocal);
            return "data:image/png;base64," + (((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.BASE64));
        }
        catch (NullPointerException ex)
        {
            return null;
        }
    }


}