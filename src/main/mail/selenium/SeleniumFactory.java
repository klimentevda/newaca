package mail.selenium;


import org.openqa.selenium.support.PageFactory;
import mail.ui.initdriver.InitialDriver;

public class SeleniumFactory {

    public static <T> T init(Class<T> type){
        return PageFactory.initElements(InitialDriver.currentDriver(), type);
    }

}
